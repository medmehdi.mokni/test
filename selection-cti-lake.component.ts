import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MatTabChangeEvent } from '@angular/material';
import { DashboardService } from 'app/services/dashboard/dashboard.service';
import { mappingFunction } from 'app/shared/filter/filter-table';
import * as moment from 'moment';

@Component({
  selector: 'app-selection-cti-lake',
  templateUrl: './selection-cti-lake.component.html',
  styleUrls: ['./selection-cti-lake.component.scss']
})
export class SelectionCtiLakeComponent implements OnInit {

  @Output() ctiTab = new EventEmitter();
  @Output() selectedRowSelectionCti = new EventEmitter();
  @Output() selectionSubTab = new EventEmitter();
  tabSelectedIndex: number = 0;
  selectedSubView: string = 'Analysis';
  selectedDataChart: any;
  subviews = ['Analysis', 'Quality'];
  timeFilterValueReports: any = '';
  timeFilterValueAdversaries: any = '';
  timeFilterValueCapabilities: any = '';
  timeFilterValueAdvisories: any = '';
  selectedRegionReports: any;
  selectedCountryReports: string;
  selectedRegionAdversaries: any;
  selectedCountryAdversaries: string;
  selectedRegionCapabilities: any;
  selectedCountryCapabilities: string;
  selectedSectorReports: string;
  selectedSectorAdversaries: string;
  selectedSectorCapabilities: string;
  selectedSectorAdvisories: string;
  selectedTypeReports: string;
  selectedTypeAdversaries: string;
  selectedTypeCapabilities: string;
  selectedTypeAdvisories: string;
  filterReports: any = '';
  filterAdversaries: any = '';
  filterCapabilities: any = '';
  filterAdvisories: any = '';
  expression_to_search_reports: any = '';
  expression_to_search_adversaries: any = '';
  expression_to_search_capabilities: any = '';
  expression_to_search_advisories: any = '';
  types = [
    { label: 'Attack Pattern', value: 'attack-pattern' },
    { label: 'Campaign', value: 'campaign' },
    { label: 'Identity', value: 'identity' },
    { label: 'Indicator', value: 'indicator' },
    { label: 'Intrusion Set', value: 'intrusion-set' },
    { label: 'Malware', value: 'malware' },
    { label: 'Observed Data', value: 'observed-data' },
    { label: 'Threat Actor', value: 'threat-actor' },
    { label: 'Threat Report', value: 'threat-report' },
    { label: 'Tool', value: 'tool' },
    { label: 'Vulnerability', value: 'vulnerability' }
  ];
  typesAdversaries = [
    { label: 'Intrusion Set', value: 'intrusion-set' },
    { label: 'Threat Actor', value: 'threat-actor' }
  ];
  typesCapabilities = [
    { label: 'Attack Pattern', value: 'attack-pattern' },
    { label: 'Tactic', value: 'tactic' }
  ];
  typesAdvisories = [
    { label: 'Vulnerability', value: 'vulnerability' },
    { label: 'Attack Pattern', value: 'attack-pattern' },
    { label: 'Malware', value: 'malware' },
    { label: 'Indicator', value: 'indicator' },
    { label: 'Tool', value: 'tool' }
  ];
  sectors = [
    { label: 'Agriculture', value: 'agriculture' },
    { label: 'Aerospace', value: 'aerospace' },
    { label: 'Automotive', value: 'automotive' },
    { label: 'Chemical', value: 'chemical' },
    { label: 'Commercial', value: 'commercial' },
    { label: 'Communications', value: 'communications' },
    { label: 'Construction', value: 'construction' },
    { label: 'Defense', value: 'defense' },
    { label: 'Education', value: 'education' },
    { label: 'Energy', value: 'energy' },
    { label: 'Entertainment', value: 'entertainment' },
    { label: 'Financial Services', value: 'financial-services' },
    { label: 'Emergency Services', value: 'emergency-services' },
    { label: 'Government Local', value: 'government-local' },
    { label: 'Government National', value: 'government-national' },
    { label: 'Government Public Services', value: 'government-public-services' },
    { label: 'Government Regional', value: 'government-regional' },
    { label: 'Healthcare', value: 'healthcare' },
    { label: 'Hospitality Leisure', value: 'hospitality-leisure' },
    { label: 'Dams', value: 'dams' },
    { label: 'Nuclear', value: 'Nuclear' },
    { label: 'Water', value: 'water' },
    { label: 'Insurance', value: 'insurance' },
    { label: 'Manufacturing', value: 'manufacturing' },
    { label: 'Mining', value: 'mining' },
    { label: 'Non Profit', value: 'non-profit' },
    { label: 'Pharmaceuticals', value: 'pharmaceuticals' },
    { label: 'Retail', value: 'retail' },
    { label: 'Technology', value: 'technology' },
    { label: 'Telecommunications', value: 'telecommunications' },
    { label: 'Transportation', value: 'transportation' },
    { label: 'Utilities', value: 'utilities' }
  ];
  regions = [
    { label: 'Africa', value: 'africa',
      countries: [
        { label: 'Algeria', value: 'dz' },
        { label: 'Angola', value: 'ao' },
        { label: 'Benin', value: 'bj' },
        { label: 'Botswana', value: 'bw' },
        { label: 'British Indian Ocean Territory', value: 'io' },
        { label: 'Burkina Faso', value: 'bf' },
        { label: 'Burundi', value: 'bi' },
        { label: 'Cabo Verde', value: 'cv' },
        { label: 'Central African Republic', value: 'cm' },
        { label: 'Chad', value: 'cf' },
        { label: 'Comoros', value: 'td' },
        { label: 'Congo', value: 'km' },
        { label: 'Côte d’Ivoire', value: 'cg' },
        { label: 'Democratic Republic of the Congo', value: 'ci' },
        { label: 'Djibouti', value: 'cd' },
        { label: 'Egypt', value: 'dj' },
        { label: 'Equatorial Guinea', value: 'eg' },
        { label: 'Eritrea', value: 'cq' },
        { label: 'Eswatini', value: 'er' },
        { label: 'Ethiopia', value: 'sz' },
        { label: 'French Southern Territories', value: 'et' },
        { label: 'Gabon', value: 'tf' },
        { label: 'Gambia', value: 'ga' },
        { label: 'Ghana', value: 'gm' },
        { label: 'Guinea', value: 'gh' },
        { label: 'Guinea-Bissau', value: 'gn' },
        { label: 'Kenya', value: 'gw' },
        { label: 'Lesotho', value: 'ke' },
        { label: 'Liberia', value: 'gw' },
        { label: 'Libya', value: 'ke' },
        { label: 'Madagascar', value: 'ls' },
        { label: 'Malawi', value: 'lr' },
        { label: 'Mali', value: 'ly' },
        { label: 'Mauritania', value: 'mg' },
        { label: 'Mauritius', value: 'mw' },
        { label: 'Mayotte', value: 'ml' },
        { label: 'Morocco', value: 'mr' },
        { label: 'Mozambique', value: 'mu' },
        { label: 'Namibia', value: 'yt' },
        { label: 'Niger', value: 'ma' },
        { label: 'Nigeria', value: 'na' },
        { label: 'Réunion', value: 'ne' },
        { label: 'Rwanda', value: 'ng' },
        { label: 'Saint Helena', value: 're' },
        { label: 'Sao Tome and Principe', value: 'rw' },
        { label: 'Senegal', value: 'sh' },
        { label: 'Seychelles', value: 'st' },
        { label: 'Sierra Leone', value: 'sn' },
        { label: 'Somalia', value: 'sc' },
        { label: 'South Africa', value: 'sl' },
        { label: 'South Sudan', value: 'so' },
        { label: 'Sudan', value: 'za' },
        { label: 'Togo', value: 'ss' },
        { label: 'Tunisia', value: 'sd' },
        { label: 'Uganda', value: 'ug' },
        { label: 'United Republic of Tanzania', value: 'tz' },
        { label: 'Western Sahara', value: 'eh' },
        { label: 'Zambia', value: 'zm' },
        { label: 'Zimbabwe', value: 'zw' }
      ]
    },
    { label: 'Americas', value: 'americas',
      countries: [
        { label: 'Anguilla', value: 'ai' },
        { label: 'Antigua and Barbuda', value: 'ag' },
        { label: 'Argentina', value: 'ar' },
        { label: 'Aruba', value: 'aw' },
        { label: 'Bahamas', value: 'bs' },
        { label: 'Barbados', value: 'bb' },
        { label: 'Belize', value: 'bz' },
        { label: 'Bermuda', value: 'bm' },
        { label: 'Bolivia', value: 'bo' },
        { label: 'Bonaire', value: '535' },
        { label: 'Bouvet Island', value: 'bv' },
        { label: 'Brazil', value: 'br' },
        { label: 'British Virgin Islands', value: 'vg' },
        { label: 'Canada', value: 'ca' },
        { label: 'Cayman Islands', value: 'ky' },
        { label: 'Chile', value: 'cl' },
        { label: 'Colombia', value: 'co' },
        { label: 'Costa Rica', value: 'cr' },
        { label: 'Cuba', value: 'cu' },
        { label: 'Curaçao', value: 'cw' },
        { label: 'Dominica', value: 'dm' },
        { label: 'Dominican Republic', value: 'do' },
        { label: 'Ecuador', value: 'ec' },
        { label: 'El Salvador', value: 'sv' },
        { label: 'Falkland Islands', value: 'fk' },
        { label: 'French Guiana', value: 'gf' },
        { label: 'Greenland', value: 'gl' },
        { label: 'Grenada', value: 'gd' },
        { label: 'Guadeloupe', value: 'gp' },
        { label: 'Guatemala', value: 'gt' },
        { label: 'Guyana', value: 'gy' },
        { label: 'Haiti', value: 'ht' },
        { label: 'Honduras', value: 'hn' },
        { label: 'Jamaica', value: 'jm' },
        { label: 'Martinique', value: 'mq' },
        { label: 'Mexico', value: 'mx' },
        { label: 'Montserrat', value: 'ms' },
        { label: 'Nicaragua', value: 'ni' },
        { label: 'Panama', value: 'pa' },
        { label: 'Paraguay', value: 'py' },
        { label: 'Peru', value: 'pe' },
        { label: 'Puerto Rico', value: 'pr' },
        { label: 'Saint Barthélemy', value: 'bl' },
        { label: 'Saint Kitts and Nevis', value: 'kn' },
        { label: 'Saint Lucia', value: 'lc' },
        { label: 'Saint Martin', value: 'mf' },
        { label: 'Saint Pierre and Miquelon', value: 'pm' },
        { label: 'Saint Vincent and the Grenadines', value: 'vc' },
        { label: 'Sint Maarten', value: 'sx' },
        { label: 'South Georgia and the South Sandwich Islands', value: 'gs' },
        { label: 'Suriname', value: 'sr' },
        { label: 'Trinidad and Tobago', value: 'tt' },
        { label: 'Turks and Caicos Islands', value: 'tc' },
        { label: 'United States of America', value: 'us' },
        { label: 'United States Virgin Islands', value: 'vi' },
        { label: 'Uruguay', value: 'uy' },
        { label: 'Venezuela ', value: 've' },{ label: 'Cayman Islands', value: 'cl' },
        { label: 'Chile', value: 'co' },
        { label: 'Colombia', value: 'cl' },
        { label: 'Costa Rica', value: 'co' },
        { label: 'Cuba', value: 'cr' },
        { label: 'Curaçao', value: 'cu' },
        { label: 'Dominica', value: 'dm' },
        { label: 'Dominican Republic', value: 'do' },
        { label: 'Ecuador', value: 'ec' },
        { label: 'El Salvador', value: 'sv' },
        { label: 'Falkland Islands', value: 'fk' },
        { label: 'French Guiana', value: 'gf' },
        { label: 'Greenland', value: 'gy' },
        { label: 'Grenada', value: 'gl' },
        { label: 'Guadeloupe', value: 'gp' },
        { label: 'Guatemala', value: 'gt' },
        { label: 'Guyana', value: 'gy' },
        { label: 'Haiti', value: 'ht' },
        { label: 'Honduras', value: 'hn' },
        { label: 'Jamaica', value: 'jm' },
        { label: 'Martinique', value: 'mq' },
        { label: 'Mexico', value: 'mx' },
        { label: 'Montserrat', value: 'ms' },
        { label: 'Nicaragua', value: 'ni' },
        { label: 'Panama', value: 'pa' },
        { label: 'Paraguay', value: 'py' },
        { label: 'Peru', value: 'pe' },
        { label: 'Puerto Rico', value: 'pr' },
        { label: 'Saint Barthélemy', value: 'bl' },
        { label: 'Saint Kitts and Nevis', value: 'kn' },
        { label: 'Saint Lucia', value: 'lc' },
        { label: 'Saint Martin', value: 'mf' },
        { label: 'Saint Pierre and Miquelon', value: 'pm' },
        { label: 'Saint Vincent and the Grenadines', value: 'vc' },
        { label: 'Sint Maarten', value: 'sx' },
        { label: 'South Georgia and the South Sandwich Islands', value: 'gs' },
        { label: 'Suriname', value: 'sr' },
        { label: 'Trinidad and Tobago', value: 'tt' },
        { label: 'Turks and Caicos Islands', value: 'tc' },
        { label: 'United States of America', value: 'us' },
        { label: 'United States Virgin Islands', value: 'vi' },
        { label: 'Uruguay', value: 'uy' },
        { label: 'Venezuela ', value: 've' }
      ]
    },
    { label: 'Asia', value: 'asia',
      countries: [
        { label: 'Afghanistan', value: 'af' },
        { label: 'Armenia', value: 'am' },
        { label: 'Azerbaijan', value: 'az' },
        { label: 'Bahrain', value: 'bh' },
        { label: 'Bangladesh', value: 'bd' },
        { label: 'Bhutan', value: 'bt' },
        { label: 'Brunei Darussalam', value: 'bn' },
        { label: 'Cambodia', value: 'kh' },
        { label: 'China', value: 'cn' },
        { label: 'China', value: '344' },
        { label: 'China', value: '446' },
        { label: 'Cyprus', value: 'cy' },
        { label: 'Democratic Peoples Republic of Korea', value: 'kp' },
        { label: 'Georgia', value: 'ge' },
        { label: 'India', value: 'in' },
        { label: 'Iran (Islamic Republic of Iran)', value: 'ir' },
        { label: 'Iraq', value: 'iq' },
        { label: 'Israel', value: 'il' },
        { label: 'Japan', value: 'jp' },
        { label: 'Jordan', value: 'jo' },
        { label: 'Kazakhstan', value: 'kz' },
        { label: 'Kuwait', value: 'kw' },
        { label: 'Kyrgyzstan', value: 'kg' },
        { label: 'Lao Peoples Democratic Republic', value: 'la' },
        { label: 'Lebanon', value: 'lb' },
        { label: 'Malaysia', value: 'my' },
        { label: 'Maldives', value: 'mv' },
        { label: 'Mongolia', value: 'mn' },
        { label: 'Myanmar', value: 'mm' },
        { label: 'Nepal', value: 'np' },
        { label: 'Oman', value: 'om' },
        { label: 'Pakistan', value: 'pk' },
        { label: 'Philippines', value: 'ph' },
        { label: 'Qatar', value: 'qa' },
        { label: 'Republic of Korea', value: 'kr' },
        { label: 'Saudi Arabia', value: 'sa' },
        { label: 'Singapore', value: 'sg' },
        { label: 'Sri Lanka', value: 'lk' },
        { label: 'State of Palestine', value: 'ps' },
        { label: 'Syrian Arab Republic', value: 'sy' },
        { label: 'Tajikistan', value: 'tj' },
        { label: 'Thailand', value: 'th' },
        { label: 'Timor-Leste', value: 'tl' },
        { label: 'Turkey', value: 'tr' },
        { label: 'Turkmenistan', value: 'tm' },
        { label: 'United Arab Emirates', value: 'ae' },
        { label: 'Uzbekistan', value: 'uz' },
        { label: 'Viet Nam', value: 'vn' },
        { label: 'Yemen', value: 'ye' }
      ]
    }, 
    { label: 'Europe', value: 'europe',
      countries: [
        { label: 'Aland Islands', value: 'ax' },
        { label: 'Albania', value: 'al' },
        { label: 'Andorra', value: 'ad' },
        { label: 'Austria', value: 'at' },
        { label: 'Belarus', value: 'by' },
        { label: 'Belgium', value: 'be' },
        { label: 'Bosnia and Herzegovina', value: 'ba' },
        { label: 'Bulgaria', value: 'bg' },
        { label: 'Croatia', value: 'hr' },
        { label: 'Czechia', value: 'cz' },
        { label: 'Denmark', value: 'dk' },
        { label: 'Estonia', value: 'ee' },
        { label: 'Faroe Islands', value: 'fo' },
        { label: 'Finland', value: 'fi' },
        { label: 'France', value: 'fr' },
        { label: 'Germany', value: 'de' },
        { label: 'Gibraltar', value: 'gi' },
        { label: 'Greece', value: 'gr' },
        { label: 'Guernsey', value: 'gg' },
        { label: 'Holy See', value: 'va' },
        { label: 'Hungary', value: 'hu' },
        { label: 'Iceland', value: 'is' },
        { label: 'Ireland', value: 'ie' },
        { label: 'Isle of Man', value: 'im' },
        { label: 'Italy', value: 'it' },
        { label: 'Jersey', value: 'je' },
        { label: 'Latvia', value: 'lv' },
        { label: 'Liechtenstein', value: 'li' },
        { label: 'Lithuania', value: 'lt' },
        { label: 'Luxembourg', value: 'lu' },
        { label: 'Malta', value: 'mt' },
        { label: 'Monaco', value: 'mc' },
        { label: 'Montenegro', value: 'me' },
        { label: 'Netherlands', value: 'nl' },
        { label: 'North Macedonia', value: 'mk' },
        { label: 'Norway', value: 'no' },
        { label: 'Poland', value: 'pl' },
        { label: 'Portugal', value: 'pt' },
        { label: 'Republic of Moldova', value: 'md' },
        { label: 'Romania', value: 'ro' },
        { label: 'Russian Federation', value: 'ru' },
        { label: 'San Marino', value: 'sm' },
        { label: 'Sark', value: '' },
        { label: 'Serbia', value: 'rs' },
        { label: 'Slovakia', value: 'sk' },
        { label: 'Slovenia', value: 'si' },
        { label: 'Spain', value: 'es' },
        { label: 'Svalbard and Jan Mayen Islands', value: 'sj' },
        { label: 'Sweden', value: 'se' }, 
        { label: 'Switzerland', value: 'ch' },
        { label: 'Ukraine', value: 'ua' }, 
        { label: 'United Kingdom of Great Britain and Northern Ireland', value: 'gb' }
      ]
    }, 
    { label: 'Oceana', value: 'oceana',
      countries: [
        { label: 'American Samoa', value: 'as' },
        { label: 'Australia', value: 'au' },
        { label: 'Christmas Island', value: 'cx' },
        { label: 'Cocos (Keeling) Islands', value: 'cc' },
        { label: 'Cook Islands', value: 'ck' },
        { label: 'Fiji', value: 'fj' },
        { label: 'French Polynesia', value: 'pf' },
        { label: 'Guam', value: 'gu' },
        { label: 'Heard Island and McDonald Islands', value: 'hm' },
        { label: 'Kiribati', value: 'ki' },
        { label: 'Marshall Islands', value: 'mh' },
        { label: 'Micronesia (Federated States of)', value: 'fm' },
        { label: 'Nauru', value: 'nr' },
        { label: 'New Caledonia', value: 'nc' },
        { label: 'New Zealand', value: 'nz' },
        { label: 'Niue', value: 'nu' },
        { label: 'Norfolk Island', value: 'nf' },
        { label: 'Northern Mariana Islands', value: 'mp' },
        { label: 'Palau', value: 'pw' },
        { label: 'Papua New Guinea', value: 'pg' },
        { label: 'Pitcairn', value: 'pn' },
        { label: 'Samoa', value: 'ws' },
        { label: 'Solomon Islands', value: 'sb' },
        { label: 'Tokelau', value: 'tk' },
        { label: 'Tonga', value: 'to' },
        { label: 'Tuvalu', value: 'tv' },
        { label: 'United States Minor Outlying Islands', value: 'um' },
        { label: 'Vanuatu', value: 'vu' },
        { label: 'Wallis and Futuna Islands', value: 'wf' }
      ]
    }
  ];
  reportsColumns = [
    { name: 'ID', checked: true, value: 'id' },
    { name: 'Created at', checked: true, value: 'created' },
    { name: 'Updated at', checked: true, value: 'modified' },
    { name: 'Name', checked: true, value: 'name' },
    { name: 'Type', checked: true, value: 'report_types' },
    { name: 'Published', checked: true, value: 'published' },
    { name: 'Labels', checked: true, value: 'labels' },
    { name: 'Confidence', checked: true, value: 'confidence' },
    { name: 'Ageing', checked: true, value: 'ageing' },
    { name: 'Sector', checked: true, value: 'sector' },

    { name: 'Region', checked: false, value: 'region' },
    { name: 'Country', checked: false, value: 'country' },
    { name: 'Created by', checked: false, value: 'created_by_ref' },
    { name: 'Revoked', checked: false, value: 'revoked' },
    { name: 'Language', checked: false, value: 'lang' }
  ];
  adversariesColumns = [
    { name: 'ID', checked: true, value: 'id' },
    { name: 'Created at', checked: true, value: 'created' },
    { name: 'Updated at', checked: true, value: 'modified' },
    { name: 'Name', checked: true, value: 'name' },
    { name: 'Aliases', checked: true, value: 'aliases' },
    { name: 'Type', checked: true, value: 'type' },
    { name: 'First Seen', checked: true, value: 'first_seen' },
    { name: 'Last Seen', checked: true, value: 'last_seen' },
    { name: 'Goals', checked: true, value: 'goals' },
    { name: 'Resource Level', checked: true, value: 'resource_level' },
    { name: 'Primary Motivation', checked: true, value: 'primary_motivation' },
    { name: 'Secondary Motivations', checked: true, value: 'secondary_motivations' },
    { name: 'Labels', checked: true, value: 'labels' },
    { name: 'Ageing', checked: true, value: 'ageing' },
    { name: 'Sector', checked: true, value: 'sector' },

    { name: 'Region', checked: false, value: 'region' },
    { name: 'Country', checked: false, value: 'country' },
    { name: 'Created by', checked: false, value: 'created_by_ref' },
    { name: 'Revoked', checked: false, value: 'revoked' },
    { name: 'Language', checked: false, value: 'lang' },
    { name: 'Confidence', checked: false, value: 'confidence' },
    { name: 'Roles', checked: false, value: 'roles' },
    { name: 'Sophistication', checked: false, value: 'sophistication' },
    { name: 'Personal Motivations', checked: false, value: 'personal_motivations' },
    { name: 'Threat Actor Types', checked: false, value: 'threat_actor_types' }
  ];
  capabilitiesColumns = [
    { name: 'ID', checked: true, value: 'id' },
    { name: 'Created at', checked: true, value: 'created' },
    { name: 'Updated at', checked: true, value: 'modified' },
    { name: 'Name', checked: true, value: 'name' },
    { name: 'Aliases', checked: true, value: 'aliases' },
    { name: 'Type', checked: true, value: 'type' },
    { name: 'Domain', checked: true, value: 'x_mitre_domains' },
    { name: 'Platforms', checked: true, value: 'x_mitre_platforms' },
    { name: 'Kill Chain Phase', checked: true, value: 'kill_chain_phases' },
    { name: 'Used by', checked: true, value: 'used_by' },
    { name: 'Uses', checked: true, value: 'uses' },
    { name: 'Labels', checked: true, value: 'labels' },
    { name: 'Ageing', checked: true, value: 'ageing' },
    { name: 'Sector', checked: true, value: 'sector' },

    { name: 'Region', checked: false, value: 'region' },
    { name: 'Country', checked: false, value: 'country' },
    { name: 'Created by', checked: false, value: 'created_by_ref' },
    { name: 'Revoked', checked: false, value: 'revoked' },
    { name: 'Language', checked: false, value: 'lang' },
    { name: 'Confidence', checked: false, value: 'confidence' },
    { name: 'ATTACK Version', checked: false, value: 'x_mitre_attack_spec_version' }
  ];
  advisoriesColumns = [
    { name: 'ID', checked: true, value: 'id' },
    { name: 'Created at', checked: true, value: 'created' },
    { name: 'Updated at', checked: true, value: 'modified' },
    { name: 'Name', checked: true, value: 'name' },
    { name: 'Type', checked: true, value: 'attack_type' },
    { name: 'Respond to', checked: true, value: 'respond_to' },
    { name: 'Course', checked: true, value: 'course' },
    { name: 'Labels', checked: true, value: 'labels' },
    { name: 'Confidence', checked: true, value: 'confidence' },
    { name: 'Ageing', checked: true, value: 'ageing' },
    { name: 'Sector', checked: true, value: 'sector' },

    { name: 'Created by', checked: false, value: 'created_by_ref' },
    { name: 'Revoked', checked: false, value: 'revoked' },
    { name: 'Language', checked: false, value: 'lang' }
  ];
  selectedReportsColumns = this.reportsColumns.filter((t) => t.checked);
  selectedAdversariesColumns = this.adversariesColumns.filter((t) => t.checked);
  selectedCapabilitiesColumns = this.capabilitiesColumns.filter((t) => t.checked);
  selectedAdvisoriesColumns = this.advisoriesColumns.filter((t) => t.checked);
  showReports: boolean = false;
  showAdversaries: boolean = false;
  showCapabilities: boolean = false;
  showAdvisories: boolean = false;
  showAfterSearchReports: boolean = false;
  showAfterSearchAdversaries: boolean = false;
  showAfterSearchCapabilities: boolean = false;
  showAfterSearchAdvisories: boolean = false;
  rowsReports: any[] = [];
  rowsAdversaries: any[] = [];
  rowsCapabilities: any[] = [];
  rowsAdvisories: any[] = [];
  totalElementReports: number;
  totalElementAdversaries: number;
  totalElementCapabilities: number;
  totalElementAdvisories: number;
  sortKeyReports: any = 'id';
  sortKeyAdversaries: any = 'id';
  sortKeyCapabilities: any = 'id';
  sortKeyAdvisories: any = 'id';
  sortOrderReports: number = -1;
  sortOrderAdversaries: number = -1;
  sortOrderCapabilities: number = -1;
  sortOrderAdvisories: number = -1;
  pageInfoReports = { 'pageSize': 5, 'pageIndex': 0 };
  pageInfoAdversaries = { 'pageSize': 5, 'pageIndex': 0 };
  pageInfoCapabilities = { 'pageSize': 5, 'pageIndex': 0 };
  pageInfoAdvisories = { 'pageSize': 5, 'pageIndex': 0 };

  constructor(
    private dashboardService: DashboardService
  ) { }

  ngOnInit() {
    this.getReportsObject(this.pageInfoReports);
    this.getAdversariesObject(this.pageInfoAdversaries);
    this.getCapabilitiesObject(this.pageInfoCapabilities);
    this.getAdvisoriesObject(this.pageInfoAdvisories);
  }

  tabChanged(tabChangeEvent: MatTabChangeEvent) {
    this.selectedDataChart = '';
    this.tabSelectedIndex = tabChangeEvent.index;
    console.log("this.tabSelectedIndex", this.tabSelectedIndex);
    this.selectionSubTab.emit(tabChangeEvent.index);
    //Fix bug 1312 MAT-TAB RESIZING ISSUE
    window.dispatchEvent(new Event('resize'));
  }

  onTimeFilterChanged(value) {
    if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 0) {
      if (value) {
        if (value.timeRange && value.timeRange !== '') {
          const filterTime = value.timeRange.toLowerCase();
          if (filterTime !== this.timeFilterValueReports) {
            this.timeFilterValueReports = filterTime;
          }
        } else if (value.startDate && value.endDate) {
          const filterTime = [(moment(value["startDate"]).format("YYYY-MM-DD")).toString(), (moment(value["endDate"]).format("YYYY-MM-DD")).toString()];
          if (filterTime !== this.timeFilterValueReports) {
            this.timeFilterValueReports = filterTime;
          }
        }
        else {
          this.timeFilterValueReports = '';
        }
      }
      this.sortOrderReports = -1;
      this.sortKeyReports = 'id';
      this.showReports = false;
      this.showAfterSearchReports = false;
      this.getReportsObject({ pageSize: this.pageInfoReports.pageSize, pageIndex: 0 });
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 1) {
      if (value) {
        if (value.timeRange && value.timeRange !== '') {
          const filterTime = value.timeRange.toLowerCase();
          if (filterTime !== this.timeFilterValueAdversaries) {
            this.timeFilterValueAdversaries = filterTime;
          }
        } else if (value.startDate && value.endDate) {
          const filterTime = [(moment(value["startDate"]).format("YYYY-MM-DD")).toString(), (moment(value["endDate"]).format("YYYY-MM-DD")).toString()];
          if (filterTime !== this.timeFilterValueAdversaries) {
            this.timeFilterValueAdversaries = filterTime;
          }
        }
        else {
          this.timeFilterValueAdversaries = '';
        }
      }
      this.sortOrderAdversaries = -1;
      this.sortKeyAdversaries = 'id';
      this.showAdversaries = false;
      this.showAfterSearchAdversaries = false;
      this.getAdversariesObject({ pageSize: this.pageInfoAdversaries.pageSize, pageIndex: 0 });
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 2) {
      if (value) {
        if (value.timeRange && value.timeRange !== '') {
          const filterTime = value.timeRange.toLowerCase();
          if (filterTime !== this.timeFilterValueCapabilities) {
            this.timeFilterValueCapabilities = filterTime;
          }
        } else if (value.startDate && value.endDate) {
          const filterTime = [(moment(value["startDate"]).format("YYYY-MM-DD")).toString(), (moment(value["endDate"]).format("YYYY-MM-DD")).toString()];
          if (filterTime !== this.timeFilterValueCapabilities) {
            this.timeFilterValueCapabilities = filterTime;
          }
        }
        else {
          this.timeFilterValueCapabilities = '';
        }
      }
      this.sortOrderCapabilities = -1;
      this.sortKeyCapabilities = 'id';
      this.showCapabilities = false;
      this.showAfterSearchCapabilities = false;
      this.getCapabilitiesObject({ pageSize: this.pageInfoCapabilities.pageSize, pageIndex: 0 });
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 3) {
      if (value) {
        if (value.timeRange && value.timeRange !== '') {
          const filterTime = value.timeRange.toLowerCase();
          if (filterTime !== this.timeFilterValueAdvisories) {
            this.timeFilterValueAdvisories = filterTime;
          }
        } else if (value.startDate && value.endDate) {
          const filterTime = [(moment(value["startDate"]).format("YYYY-MM-DD")).toString(), (moment(value["endDate"]).format("YYYY-MM-DD")).toString()];
          if (filterTime !== this.timeFilterValueAdvisories) {
            this.timeFilterValueAdvisories = filterTime;
          }
        }
        else {
          this.timeFilterValueAdvisories = '';
        }
      }
      this.sortOrderAdvisories = -1;
      this.sortKeyAdvisories = 'id';
      this.showAdvisories = false;
      this.showAfterSearchAdvisories = false;
      this.getAdvisoriesObject({ pageSize: this.pageInfoAdvisories.pageSize, pageIndex: 0 });
    }
    // else if (this.selectedSubView === 'Quality') {
    //   this.showQuality = false;
    //   this.showQualityAfterSearch = false;
    //   this.getVulnerabilityQualityObject({ pageSize: this.QualitypageInfo.pageSize, pageIndex: 0 });
    // }
  }

  startSearch(filter) {
    if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 0) {
      this.sortOrderReports = 1;
      this.sortKeyReports = 'id';
      this.showReports = false;
      this.showAfterSearchReports = false;
      this.expression_to_search_reports = filter;
      this.getReportsObject({ pageSize: this.pageInfoReports.pageSize, pageIndex: 0 });
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 1) {
      this.sortOrderAdversaries = 1;
      this.sortKeyAdversaries = 'id';
      this.showAdversaries = false;
      this.showAfterSearchAdversaries = false;
      this.expression_to_search_adversaries = filter;
      this.getAdversariesObject({ pageSize: this.pageInfoAdversaries.pageSize, pageIndex: 0 });
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 2) {
      this.sortOrderCapabilities = 1;
      this.sortKeyCapabilities = 'id';
      this.showCapabilities = false;
      this.showAfterSearchCapabilities = false;
      this.expression_to_search_capabilities = filter;
      this.getCapabilitiesObject({ pageSize: this.pageInfoCapabilities.pageSize, pageIndex: 0 });
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 3) {
      this.sortOrderAdvisories = 1;
      this.sortKeyAdvisories = 'id';
      this.showAdvisories = false;
      this.showAfterSearchAdvisories = false;
      this.expression_to_search_advisories = filter;
      this.getAdvisoriesObject({ pageSize: this.pageInfoAdvisories.pageSize, pageIndex: 0 });
    }
    // else if (this.selectedSubView === 'Quality') {
    //   this.showQuality = false;
    //   this.showQualityAfterSearch = false;
    //   this.expression_to_search_quality = filter;
    //   this.getVulnerabilityQualityObject({ pageSize: this.QualitypageInfo.pageSize, pageIndex: 0 });
    // }
  }

  getReportsColumns($event) {
    this.selectedReportsColumns = $event;
  }

  getAdversariesColumns($event) {
    this.selectedAdversariesColumns = $event;
  }

  getCapabilitiesColumns($event) {
    this.selectedCapabilitiesColumns = $event;
  }

  getAdvisoriesColumns($event) {
    this.selectedAdvisoriesColumns = $event;
  }

  onSort(event) {
    if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 0) {
      this.sortKeyReports = event.sorts[0].prop;
      this.sortOrderReports = event.sorts[0].dir === 'asc' ? 1 : -1;
      this.getReportsObject({ pageSize: this.pageInfoReports.pageSize, pageIndex: 0 });
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 1) {
      this.sortKeyAdversaries = event.sorts[0].prop;
      this.sortOrderAdversaries = event.sorts[0].dir === 'asc' ? 1 : -1;
      this.getAdversariesObject({ pageSize: this.pageInfoAdversaries.pageSize, pageIndex: 0 })
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 2) {
      this.sortKeyCapabilities = event.sorts[0].prop;
      this.sortOrderCapabilities = event.sorts[0].dir === 'asc' ? 1 : -1;
      this.getCapabilitiesObject({ pageSize: this.pageInfoCapabilities.pageSize, pageIndex: 0 })
    }
    else if (this.selectedSubView === 'Analysis' && this.tabSelectedIndex == 3) {
      this.sortKeyAdvisories = event.sorts[0].prop;
      this.sortOrderAdvisories = event.sorts[0].dir === 'asc' ? 1 : -1;
      this.getAdvisoriesObject({ pageSize: this.pageInfoAdvisories.pageSize, pageIndex: 0 })
    }
  }

  getReportsObject(pageInfo) {
    const mapping = {
      'ID': 'id',
      'Created at': 'created',
      'Updated at': 'modified',
      'Name': 'name',
      'Type': 'report_types',
      'Published': 'published',
      'Labels': 'labels',
      'Confidence': 'confidence',
      'Ageing': 'ageing',
      'Sector': 'sector',
      'Region': 'region',
      'Country': 'country',
      'Created by': 'created_by_ref',
      'Revoked': 'revoked',
      'Language': 'lang'
    }

    this.pageInfoReports = {
      'pageSize': pageInfo.pageSize,
      'pageIndex': pageInfo.pageIndex
    };

    const filterValue: string = this.expression_to_search_reports === '' ? this.expression_to_search_reports : mappingFunction(this.expression_to_search_reports, mapping);
    const input = {
      per_page: pageInfo.pageSize,
      page_number: pageInfo.pageIndex + 1,
      filter_expression: filterValue,
      date: this.timeFilterValueReports,
      key: this.sortKeyReports,
      order: this.sortOrderReports,
      type: this.selectedTypeReports ? this.selectedTypeReports : '',
      sector: this.selectedSectorReports ? this.selectedSectorReports : '',
      region: this.selectedRegionReports ? this.selectedRegionReports.value : '',
      country: this.selectedCountryReports ? this.selectedCountryReports : ''
    }
    this.showReports = false;

    this.dashboardService.getReportsList(input).subscribe(res => {
      if (res.status && res.data != []) {
        this.rowsReports = res.data;
        this.totalElementReports = res.total;
      }
      else {
        this.rowsReports = [];
        this.totalElementReports = 0;
      }
      this.showReports = true;
      this.showAfterSearchReports = true
    });
  }

  getAdversariesObject(pageInfo) {
    const mapping = {
      'ID': 'id',
      'Created at': 'created',
      'Updated at': 'modified',
      'Name': 'name',
      'Aliases': 'aliases',
      'Type': 'type',
      'First Seen': 'first_seen',
      'Last Seen': 'last_seen',
      'Goals': 'goals',
      'Resource Level': 'resource_level',
      'Primary Motivation': 'primary_motivation',
      'Secondary Motivations': 'secondary_motivations',
      'Labels': 'labels',
      'Ageing': 'ageing',
      'Sector': 'sector',
      'Region': 'region',
      'Country': 'country',
      'Created by': 'created_by_ref',
      'Revoked': 'revoked',
      'Language': 'lang',
      'Confidence': 'confidence',
      'Roles': 'roles',
      'Sophistication': 'sophistication',
      'Personal Motivations': 'personal_motivations',
      'Threat Actor Types': 'threat_actor_types'
    }

    this.pageInfoAdversaries = {
      'pageSize': pageInfo.pageSize,
      'pageIndex': pageInfo.pageIndex
    };

    const filterValue: string = this.expression_to_search_adversaries === '' ? this.expression_to_search_adversaries : mappingFunction(this.expression_to_search_adversaries, mapping);
    const input = {
      per_page: pageInfo.pageSize,
      page_number: pageInfo.pageIndex + 1,
      filter_expression: filterValue,
      date: this.timeFilterValueAdversaries,
      key: this.sortKeyAdversaries,
      order: this.sortOrderAdversaries,
      type: this.selectedTypeAdversaries ? this.selectedTypeAdversaries : '',
      sector: this.selectedSectorAdversaries ? this.selectedSectorAdversaries : '',
      region: this.selectedRegionAdversaries ? this.selectedRegionAdversaries.value : '',
      country: this.selectedCountryAdversaries ? this.selectedCountryAdversaries : ''
    }
    this.showAdversaries = false;

    this.dashboardService.getAdversariesList(input).subscribe(res => {
      if (res.status && res.data != []) {
        this.rowsAdversaries = res.data;
        this.totalElementAdversaries = res.total;
      }
      else {
        this.rowsAdversaries = [];
        this.totalElementAdversaries = 0;
      }
      this.showAdversaries = true;
      this.showAfterSearchAdversaries = true
    });
  }

  getCapabilitiesObject(pageInfo) {
    const mapping = {
      'ID': 'id',
      'Created at': 'created',
      'Updated at': 'modified',
      'Name': 'name',
      'Aliases': 'aliases',
      'Type': 'type',
      'Domain': 'x_mitre_domains',
      'Platforms': 'x_mitre_platforms',
      'Kill Chain Phase': 'kill_chain_phases',
      'Used by': 'used_by',
      'Uses': 'uses',
      'Labels': 'labels',
      'Ageing': 'ageing',
      'Sector': 'sector',
      'Region': 'region',
      'Country': 'country',
      'Created by': 'created_by_ref',
      'Revoked': 'revoked',
      'Language': 'lang',
      'Confidence': 'confidence',
      'ATTACK Version': 'x_mitre_attack_spec_version'
    }

    this.pageInfoCapabilities = {
      'pageSize': pageInfo.pageSize,
      'pageIndex': pageInfo.pageIndex
    };

    const filterValue: string = this.expression_to_search_capabilities === '' ? this.expression_to_search_capabilities : mappingFunction(this.expression_to_search_capabilities, mapping);
    const input = {
      per_page: pageInfo.pageSize,
      page_number: pageInfo.pageIndex + 1,
      filter_expression: filterValue,
      date: this.timeFilterValueCapabilities,
      key: this.sortKeyCapabilities,
      order: this.sortOrderCapabilities,
      type: this.selectedTypeCapabilities ? this.selectedTypeCapabilities : '',
      sector: this.selectedSectorCapabilities ? this.selectedSectorCapabilities : '',
      region: this.selectedRegionCapabilities ? this.selectedRegionCapabilities.value : '',
      country: this.selectedCountryCapabilities ? this.selectedCountryCapabilities : ''
    }
    this.showCapabilities = false;

    this.dashboardService.getCapabilitiesList(input).subscribe(res => {
      if (res.status && res.data != []) {
        this.rowsCapabilities = res.data;
        this.totalElementCapabilities = res.total;
      }
      else {
        this.rowsCapabilities = [];
        this.totalElementCapabilities = 0;
      }
      this.showCapabilities = true;
      this.showAfterSearchCapabilities = true
    });
  }

  getAdvisoriesObject(pageInfo) {
    const mapping = {
      'ID': 'id',
      'Created at': 'created',
      'Updated at': 'modified',
      'Name': 'name',
      'Type': 'attack_type',
      'Respond to': 'respond_to',
      'Course': 'course',
      'Labels': 'labels',
      'Confidence': 'confidence',
      'Ageing': 'ageing',
      'Sector': 'sector',
      'Created by': 'created_by_ref',
      'Revoked': 'revoked',
      'Language': 'lang'
    }

    this.pageInfoAdvisories = {
      'pageSize': pageInfo.pageSize,
      'pageIndex': pageInfo.pageIndex
    };

    const filterValue: string = this.expression_to_search_advisories === '' ? this.expression_to_search_advisories : mappingFunction(this.expression_to_search_advisories, mapping);
    const input = {
      per_page: pageInfo.pageSize,
      page_number: pageInfo.pageIndex + 1,
      filter_expression: filterValue,
      date: this.timeFilterValueAdvisories,
      key: this.sortKeyAdvisories,
      order: this.sortOrderAdvisories,
      type: this.selectedTypeAdvisories ? this.selectedTypeAdvisories : '',
      sector: this.selectedSectorAdvisories ? this.selectedSectorAdvisories : ''
    }
    this.showAdvisories = false;

    this.dashboardService.getAdvisoriesList(input).subscribe(res => {
      if (res.status && res.data != []) {
        this.rowsAdvisories = res.data;
        this.totalElementAdvisories = res.total;
      }
      else {
        this.rowsAdvisories = [];
        this.totalElementAdvisories = 0;
      }
      this.showAdvisories = true;
      this.showAfterSearchAdvisories = true
    });
  }

  showCorrelation(row) {
    this.ctiTab.emit(2);
    this.selectedRowSelectionCti.emit(row);
  }

}
